package org.ryubou;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class RyubouPlugin extends JavaPlugin implements Listener {

	@Override
	public void onEnable() {
		super.onEnable();

		PluginManager manager = getServer().getPluginManager();
		manager.registerEvents(this, this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("ryubou")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				p.sendMessage("Hello!");
				return true;
			}
		}

		return false;
	}

	/**
	 * プレイヤーがクリック、または右クリックした時にこのメソッドが呼び出されます
	 * 引数の「event」には、クリックが発生した時の情報が入っています
	 */
	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent event) {

		//イベント情報(event)から、手に持っているアイテム(itemStack)を取得します
		ItemStack itemStack = event.getItem();

		//手に何も持っていない場合は、itemStackはnullになります
		//その場合、処理を終了します
		if (itemStack == null) {
			return;
		}

		//アイテムのタイプをチェックします
		//コンパスではなかった場合、処理を終了します
		if (itemStack.getType() != Material.BLAZE_ROD) {
			return;
		}
		ItemMeta itemMeta = itemStack.getItemMeta();

		if (!(itemMeta.getDisplayName().equals("ポテト") )) {
			return;
		}

		//イベント情報(event)tから、クリックを実行したプレイヤー(player)を取得します
		Player player = event.getPlayer();

		//プレイヤー(player)から、目の位置情報(eyeLocation)を取得します
		Location eyeLocation = player.getEyeLocation();

		//目の位置情報(eyeLocation)から、目の向いている方向(direction)を取得します
		//このdirectionは、X Y Zの３つの要素を持つベクターオブジェクトと呼ばれるものです
		//ベクターオブジェクトで３次元角度を表す場合は、X Y Zの合計値がちょうど1になるように調節されています
		//これを正規化されたベクターオブジェクトと呼びます
		//正規化されたベクターオブジェクト ＝ ３次元角度を表すオブジェクト ということです
		Vector vector = eyeLocation.getDirection();

		//正規化されたベクターオブジェクト(vector)を10倍します
		//ベクターオブジェクトを10倍した場合、 X Y Z の全ての数値が10倍されます
		//正規化されたベクターオブジェクトは、３次元角度を表すオブジェクトだと言いましたが
		//10倍したことで、３次元角度を表すオブジェクト、かつ、距離を表すオブジェクトになりました
		//つまり、directionオブジェクトは今、視線の先に10進んだ距離を表しています
		vector.multiply(10);

		//プレイヤー(player)から、プレイヤーの位置情報(location)を取得します
		Location location = player.getLocation();

		//プレイヤーの位置情報(location)に、先ほど計算したベクターオブジェクト(vector)を加算します
		//マイクラの位置情報には、こんな感じでベクターオブジェクトを足したり引いたりできます
		location.add(vector);

		//プレイヤーを、先ほど計算した位置(location)にテレポートします
		player.teleport(location);
	}
}